<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Videocard;

class Farm extends Model
{
    public static $farms = [
        0 => [
            'name' => 'Дом',
            'price' => '0',
            'maxTdp' => 1500,
            'image' => 'id-0.png'
        ],
    ];

    public static $motherboards = [
        1 => [
            'brend' => 'ASUS',
            'name' => 'B85M-G',
            'socket' => 'LGA1150',
            'chipset' => 'Intel B85',
            'memory_type' => 'DDR3',
            'memory_slots' => 4,
            'memory_value' => 32,
            'form' => 'microATX',
            'price' => 64,
            'image' => 'id-1.jpg'
        ], 2 => [
            'brend' => 'ASRock',
            'name' => 'B250M Pro4',
            'socket' => 'LGA1151',
            'chipset' => 'Intel B250',
            'memory_type' => 'DDR4',
            'memory_slots' => 4,
            'memory_value' => 64,
            'form' => 'microATX',
            'price' => 78,
            'image' => 'id-2.jpg'
        ],
    ];

    public static $processors = [
        1 => [
            'brend' => 'Intel',
            'name' => 'Celeron G1840',
            'family' => 'Intel Pentium Dual-Core',
            'socket' => 'LGA1150',
            'generation' => 'Haswell',
            'cores' => 2,
            'threads' => 2,
            'frequency' => 2800,
            'tdp' => 53,
            'price' => 73,
            'image' => 'id-1.jpg'
        ], 2 => [
            'brend' => 'Intel',
            'name' => 'Pentium G3260',
            'family' => 'Intel Pentium Dual-Core',
            'socket' => 'LGA1150',
            'generation' => 'Haswell',
            'cores' => 2,
            'threads' => 2,
            'frequency' => 3300,
            'tdp' => 53,
            'price' => 71,
            'image' => 'id-2.jpg'
        ], 3 => [
            'brend' => 'Intel',
            'name' => 'Celeron G3900',
            'family' => 'Intel Celeron',
            'socket' => 'LGA1151',
            'generation' => 'Skylake',
            'cores' => 2,
            'threads' => 2,
            'frequency' => 2800,
            'tdp' => 51,
            'price' => 32,
            'image' => 'id-1.jpg'
        ], 4 => [
            'brend' => 'Intel',
            'name' => 'Celeron G3930',
            'family' => 'Intel Celeron',
            'socket' => 'LGA1151',
            'generation' => 'Kaby Lake',
            'cores' => 2,
            'threads' => 2,
            'frequency' => 2900,
            'tdp' => 51,
            'price' => 33,
            'image' => 'id-1.jpg'
        ], 5 => [
            'brend' => 'Intel',
            'name' => 'Pentium G4400',
            'family' => 'Intel Pentium Dual-Core',
            'socket' => 'LGA1151',
            'generation' => 'Skylake',
            'cores' => 2,
            'threads' => 2,
            'frequency' => 3300,
            'tdp' => 54,
            'price' => 52,
            'image' => 'id-2.jpg'
        ],
    ];

    public static $drives = [
        1 => [
            'brend' => 'Western Digital',
            'name' => 'WD Blue 1 TB',
            'type' => 'HDD',
            'form' => '3.5"',
            'value' => 1000,
            'speed' => 7200,
            'connection' => 'SATA 6Gbit/s',
            'price' => 46,
            'image' => 'id-1.jpg'
        ],
    ];

    public static $powersupplies = [
        1 => [
            'brend' => 'ExeGate',
            'name' => 'ATX-1000PPX',
            'form' => 'ATX',
            'power' => 1000,
            'price' => 80,
            'image' => 'id-1.jpg'
        ], 2 => [
            'brend' => 'GameMax',
            'name' => 'GM1050',
            'form' => 'ATX',
            'power' => 1050,
            'price' => 98,
            'image' => 'id-2.jpg'
        ],
    ];

    public static $ram = [
        1 => [
            'brend' => 'HyperX',
            'name' => 'HX316C10F*/4',
            'type' => 'DDR3',
            'value' => 4,
            'frequency' => 1600,
            'price' => 44,
            'image' => 'id-1.jpg'
        ], 2 => [
            'brend' => 'Samsung',
            'name' => 'DDR4 2400 DIMM 4Gb',
            'type' => 'DDR4',
            'value' => 4,
            'frequency' => 2400,
            'price' => 52,
            'image' => 'id-2.jpg'
        ],
    ];

    protected $fillable = [
    	'type_id', 'owner_id', 'motherboard_id', 'cpu_id', 'drive_id', 'powersupply_id', 'ram_id'
    ];


    public function getVideocardsInfo(){
    	$videocards = Videocard::where('farm_id', $this->id)->get();

    	$count = count($videocards);
    	$tdp = 0;
        $hashrate = 0;

    	foreach($videocards as $videocard){
    		$tdp += Videocard::$videocards[$videocard->type_id]['tdp'];
            $hashrate += Videocard::$videocards[$videocard->type_id]['hashrate'];
    	}

        if($this->cpu_id != 0) $tdp += Farm::$processors[$this->cpu_id]['tdp'];

    	return [
    		'count' => $count,
    		'tdp' => $tdp,
    		'hashrate' => $hashrate
    	];
    }

    public function isWork(){
        $needed = [
            'motherboard_id', 'cpu_id', 'drive_id', 'powersupply_id', 'ram_id'
        ];

        foreach($needed as $item){
            if($this->$item == 0) return 'Не хватает комплектующих';
        }

        if($this->getVideocardsInfo()['tdp'] > Farm::$powersupplies[$this->powersupply_id]['power']){
            return 'Слишком слабый блок питания';
        }

        return true;
    }
}
