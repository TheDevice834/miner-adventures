<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Farm;
use App\Videocard;
use Auth;

class ShopController extends Controller
{
    public function index(){
    	$items = [];
        $categories = $this->categories();

        foreach($categories as $category){
            if(isset($category['method'])){
                $method = $category['method'];
                $items = array_merge($items, $this->$method());
            }
        }

    	return view('shop.index', [
    		'title' => 'Магазин',
    		'items' => $items,
            'categories' => $categories
    	]);
    }

    public function category($category_name){
        $categories = $this->categories();

        $finded = false;
        foreach($categories as $category){
            if($category['name'] == $category_name){
                $method = $category['method'];
                $finded = $this->$method();
                $title = $category['title'];

                break;
            }
        }
        if(!$finded && $finded != []) return 404;

        return view('shop.index', [
            'title' => $title,
            'items' => $finded,
            'categories' => $categories
        ]);
    }

    public function buy(Request $request, $type, $id){
        $user = Auth::user();

        switch($type){
            case 'farms':
                if(!isset(Farm::$farms[$id])) return redirect()->route('shop');
                $price = Farm::$farms[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if($object = Farm::create([
                    'owner_id' => $user->id,
                    'type_id' => $id
                ])){
                    $user->dollars = $user->dollars - $price;
                    $user->save();

                    return redirect()->route('shop')->with('success', 'Вы купили ферму!');
                }
                break;

            case 'videocards':
                if(!isset(Videocard::$videocards[$id])) return redirect()->route('shop');
                $price = Videocard::$videocards[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if(!$farm = $request->input('farm')){
                    $farms = Farm::where('owner_id', $user->id)->orderBy('id', 'desc')->get();

                    return view('shop.select-farm', [
                        'title' => 'Выбор фермы',
                        'farms' => $farms,
                        'types' => Farm::$farms,
                        'text' => 'помещена видеокарта'
                    ]);
                }
                $farm = intval($farm);

                if(!$farm = Farm::find($farm)) abort(404);

                if($farm->owner_id != $user->id) abort(404);

                if($object = Videocard::create([
                    'farm_id' => $farm->id,
                    'type_id' => $id
                ])){
                    $user->dollars = $user->dollars - $price;
                    $user->save();

                    return redirect()->route('shop')->with('success', 'Вы купили видеокарту для фермы <b>#'.$farm->id.'</b>!');
                }
                break;

            case 'cpu':
                if(!isset(Farm::$processors[$id])) return redirect()->route('shop');
                $price = Farm::$processors[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if(!$farm = $request->input('farm')){
                    $farms = Farm::where('owner_id', $user->id)->orderBy('id', 'desc')->get();

                    return view('shop.select-farm', [
                        'title' => 'Выбор фермы',
                        'farms' => $farms,
                        'types' => Farm::$farms,
                        'text' => 'помещен процессор'
                    ]);
                }
                $farm = intval($farm);
                
                if(!$farm = Farm::find($farm)) abort(404);

                if($farm->owner_id != $user->id) abort(404);

                if($farm->motherboard_id == 0){
                    return redirect()->route('shop')->with('error', 'На данной ферме не установлена материнская плата.');
                }

                if(Farm::$motherboards[$farm->motherboard_id]['socket'] != Farm::$processors[$id]['socket']){
                    return redirect()->route('shop')->with('error', 'Сокет процессора не соответствует сокету материнской платы.');
                }

                $farm->cpu_id = $id;
                $farm->save();
                
                $user->dollars = $user->dollars - $price;
                $user->save();

                return redirect()->route('shop')->with('success', 'Вы купили процессор для фермы <b>#'.$farm->id.'</b>!');
                
                break;

            case 'motherboards':
                if(!isset(Farm::$motherboards[$id])) return redirect()->route('shop');
                $price = Farm::$motherboards[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if(!$farm = $request->input('farm')){
                    $farms = Farm::where('owner_id', $user->id)->orderBy('id', 'desc')->get();

                    return view('shop.select-farm', [
                        'title' => 'Выбор фермы',
                        'farms' => $farms,
                        'types' => Farm::$farms,
                        'text' => 'помещена материнская плата'
                    ]);
                }
                $farm = intval($farm);
                
                if(!$farm = Farm::find($farm)) abort(404);

                if($farm->owner_id != $user->id) abort(404);

                $farm->motherboard_id = $id;
                $farm->save();
                
                $user->dollars = $user->dollars - $price;
                $user->save();

                return redirect()->route('shop')->with('success', 'Вы купили материнскую плату для фермы <b>#'.$farm->id.'</b>!');
                
                break;

            case 'powersupplies':
                if(!isset(Farm::$powersupplies[$id])) return redirect()->route('shop');
                $price = Farm::$powersupplies[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if(!$farm = $request->input('farm')){
                    $farms = Farm::where('owner_id', $user->id)->orderBy('id', 'desc')->get();

                    return view('shop.select-farm', [
                        'title' => 'Выбор фермы',
                        'farms' => $farms,
                        'types' => Farm::$farms,
                        'text' => 'помещен блок питания'
                    ]);
                }
                $farm = intval($farm);
                
                if(!$farm = Farm::find($farm)) abort(404);

                if($farm->owner_id != $user->id) abort(404);

                $farm->powersupply_id = $id;
                $farm->save();
                
                $user->dollars = $user->dollars - $price;
                $user->save();

                return redirect()->route('shop')->with('success', 'Вы купили блок питания для фермы <b>#'.$farm->id.'</b>!');
                
                break;

            case 'drives':
                if(!isset(Farm::$drives[$id])) return redirect()->route('shop');
                $price = Farm::$drives[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if(!$farm = $request->input('farm')){
                    $farms = Farm::where('owner_id', $user->id)->orderBy('id', 'desc')->get();

                    return view('shop.select-farm', [
                        'title' => 'Выбор фермы',
                        'farms' => $farms,
                        'types' => Farm::$farms,
                        'text' => 'помещен накопитель'
                    ]);
                }
                $farm = intval($farm);
                
                if(!$farm = Farm::find($farm)) abort(404);

                if($farm->owner_id != $user->id) abort(404);

                if($farm->motherboard_id == 0){
                    return redirect()->route('shop')->with('error', 'На данной ферме не установлена материнская плата.');
                }

                $farm->drive_id = $id;
                $farm->save();
                
                $user->dollars = $user->dollars - $price;
                $user->save();

                return redirect()->route('shop')->with('success', 'Вы купили накопитель для фермы <b>#'.$farm->id.'</b>!');
                
                break;

            case 'ram':
                if(!isset(Farm::$ram[$id])) return redirect()->route('shop');
                $price = Farm::$ram[$id]['price'];
                if($price > $user->dollars) return redirect()->route('shop')->with('error', 'Недостаточно денег для совершения покупки.');

                if(!$farm = $request->input('farm')){
                    $farms = Farm::where('owner_id', $user->id)->orderBy('id', 'desc')->get();

                    return view('shop.select-farm', [
                        'title' => 'Выбор фермы',
                        'farms' => $farms,
                        'types' => Farm::$farms,
                        'text' => 'помещена оперативная память'
                    ]);
                }
                $farm = intval($farm);
                
                if(!$farm = Farm::find($farm)) abort(404);

                if($farm->owner_id != $user->id) abort(404);

                if($farm->motherboard_id == 0){
                    return redirect()->route('shop')->with('error', 'На данной ферме не установлена материнская плата.');
                }

                if(Farm::$motherboards[$farm->motherboard_id]['memory_type'] != Farm::$ram[$id]['type']){
                    return redirect()->route('shop')->with('error', 'Тип памяти материнской платы не соответствует типу памяти ОЗУ.');
                }

                $farm->ram_id = $id;
                $farm->save();
                
                $user->dollars = $user->dollars - $price;
                $user->save();

                return redirect()->route('shop')->with('success', 'Вы купили накопитель для фермы <b>#'.$farm->id.'</b>!');
                
                break;

            default:
                return redirect()->route('shop');
        }
    }

    private function categories($category = null){
        $counts = [
            'videocards' => count($this->getVideocards()),
            'farms' => count($this->getFarms()),
            'motherboards' => count($this->getMotherBoards()),
            'processors' => count($this->getProccessors()),
            'powersupplies' => count($this->getPowerSupplies()),
            'drives' => count($this->getDrives()),
            'ram' => count($this->getRam())
        ];

        $mainCount = 0;

        foreach($counts as $count){
            $mainCount += $count;
        }

        $categories = [
            [
                'title' => 'Все',
                'name' => 'all',
                'link' => route('shop'),
                'count' => $mainCount
            ], [
                'title' => 'Фермы',
                'name' => 'farms',
                'link' => route('shop.category', ['category_name' => 'farms']),
                'count' => $counts['farms'],
                'method' => 'getFarms'
            ], [
                'title' => 'Видеокарты',
                'name' => 'gpu',
                'link' => route('shop.category', ['category_name' => 'gpu']),
                'count' => $counts['videocards'],
                'method' => 'getVideocards'
            ], [
                'title' => 'Материнские платы',
                'name' => 'motherboards',
                'link' => route('shop.category', ['category_name' => 'motherboards']),
                'count' => $counts['motherboards'],
                'method' => 'getMotherBoards'
            ], [
                'title' => 'Процессоры',
                'name' => 'cpu',
                'link' => route('shop.category', ['category_name' => 'cpu']),
                'count' => $counts['processors'],
                'method' => 'getProccessors'
            ], [
                'title' => 'Блоки питания',
                'name' => 'powersupplies',
                'link' => route('shop.category', ['category_name' => 'powersupplies']),
                'count' => $counts['powersupplies'],
                'method' => 'getPowerSupplies'
            ], [
                'title' => 'Жесткие диски, SSD',
                'name' => 'drives',
                'link' => route('shop.category', ['category_name' => 'drives']),
                'count' => $counts['drives'],
                'method' => 'getDrives'
            ], [
                'title' => 'Оперативная память',
                'name' => 'ram',
                'link' => route('shop.category', ['category_name' => 'ram']),
                'count' => $counts['ram'],
                'method' => 'getRam'
            ],
        ];

        if(!isset($category)) return $categories;
        else return $categories[$category];
    }

    private function getFarms(){
        $farms = [];

        foreach(Farm::$farms as $key => $farm){
            if($key != 0){
                $farms[] = [
                    'name' => $farm['name'],
                    'category' => 1,
                    'price' => $farm['price'],
                    'advanced' => [
                        'Макс. потребление' => $farm['maxTdp'].' Ватт',
                    ],
                    'advancedModal' => [
                        'Макс. потребление' => $farm['maxTdp'].' Ватт',
                    ],
                    'image' => $farm['image'],
                    'type' => 'farms'
                ];
            }
        }

        return $farms;
    }

    public function getVideocards(){
        $videocards = [];
        foreach(Videocard::$videocards as $key => $videocard){
            $videocards[] = [
                'name' => $videocard['brend'].' '.$videocard['model'],
                'category' => 2,
                'price' => $videocard['price'],
                'advanced' => [
                    'Хэшрейт' => $videocard['hashrate'].' Gh/s',
                    'Энергопотребление' => $videocard['tdp'].' Ватт',
                ],
                'advancedModal' => [
                    'Брэнд' => $videocard['brend'],
                    'Модель' => $videocard['model'],
                    'Количество видеопамяти' => $videocard['memory_value'].' Gb',
                    'Тип видеопамяти' => $videocard['memory_type'],
                    'Частота графического процессора' => $videocard['gpu_frequency'].' Mhz',
                    'Энергопотребление' => $videocard['tdp'].' Ватт',
                    'Хэшрейт' => $videocard['hashrate'].' Gh/s',
                ],
                'image' => $videocard['image'],
                'type' => 'videocards',
                'id' => $key,
                'item' => $videocard
            ];
        }

        return $videocards;
    }

    public function getMotherBoards(){
        $motherboards = [];
        foreach(Farm::$motherboards as $key => $motherboard){
            $motherboards[] = [
                'name' => $motherboard['brend'].' '.$motherboard['name'],
                'category' => 3,
                'price' => $motherboard['price'],
                'advanced' => [
                    'Сокет' => $motherboard['socket'],
                    'Тип памяти' => $motherboard['memory_type'],
                ],
                'advancedModal' => [
                    'Брэнд' => $motherboard['brend'],
                    'Модель' => $motherboard['name'],
                    'Сокет' => $motherboard['socket'],
                    'Чипсет' => $motherboard['chipset'],
                    'Тип памяти' => $motherboard['memory_type'],
                    'Количество слотов' => $motherboard['memory_slots'],
                    'Максимальный объем памяти' => $motherboard['memory_value'].' ГБ',
                    'Форм-фактор' => $motherboard['form']
                ],
                'image' => $motherboard['image'],
                'type' => 'motherboards',
                'id' => $key,
                'item' => $motherboard
            ];
        }

        return $motherboards;
    }

    public function getProccessors(){
        $processors = [];
        foreach(Farm::$processors as $key => $processor){
            $processors[] = [
                'name' => $processor['brend'].' '.$processor['name'],
                'category' => 4,
                'price' => $processor['price'],
                'advanced' => [
                    'Сокет' => $processor['socket'],
                    'Энергопотребление' => $processor['tdp'].' Ватт',
                ],
                'advancedModal' => [
                    'Брэнд' => $processor['brend'],
                    'Семейство' => $processor['family'],
                    'Поколение' => $processor['generation'],
                    'Сокет' => $processor['socket'],
                    'Модель' => $processor['name'],
                    'Ядра' => $processor['cores'],
                    'Потоки' => $processor['threads'],
                    'Частота' => $processor['frequency'].' Mhz',
                    'Энергопотребление' => $processor['tdp'].' Ватт',
                ],
                'image' => $processor['image'],
                'type' => 'cpu',
                'id' => $key,
                'item' => $processor
            ];
        }

        return $processors;
    }

    public function getDrives(){
        $drives = [];
        foreach(Farm::$drives as $key => $drive){
            $drives[] = [
                'name' => $drive['brend'].' '.$drive['name'],
                'category' => 6,
                'price' => $drive['price'],
                'advanced' => [
                    'Объем' => $drive['value'].' Гб',
                    'Тип' => $drive['type']
                ],
                'advancedModal' => [
                    'Брэнд' => $drive['brend'],
                    'Модель' => $drive['name'],
                    'Тип' => $drive['type'],
                    'Объем' => $drive['value'].' Гб',
                    'Форм-фактор' => $drive['form'],
                    'Скорость вращения' => $drive['speed'],
                    'Подключение' => $drive['connection']
                ],
                'image' => $drive['image'],
                'type' => 'drives',
                'id' => $key,
                'item' => $drive
            ];
        }

        return $drives;
    }

    public function getPowerSupplies(){
        $powersupplies = [];
        foreach(Farm::$powersupplies as $key => $powersupply){
            $powersupplies[] = [
                'name' => $powersupply['brend'].' '.$powersupply['name'],
                'category' => 5,
                'price' => $powersupply['price'],
                'advanced' => [
                    'Мощность' => $powersupply['power'].' Вт',
                    'Форм-фактор' => $powersupply['form']
                ],
                'advancedModal' => [
                    'Брэнд' => $powersupply['brend'],
                    'Модель' => $powersupply['name'],
                    'Мощность' => $powersupply['power'].' Ватт',
                    'Форм-фактор' => $powersupply['form']
                ],
                'image' => $powersupply['image'],
                'type' => 'powersupplies',
                'id' => $key,
                'item' => $powersupply
            ];
        }

        return $powersupplies;
    }

    public function getRam(){
        $ram = [];
        foreach(Farm::$ram as $key => $item){
            $ram[] = [
                'name' => $item['brend'].' '.$item['name'],
                'category' => 7,
                'price' => $item['price'],
                'advanced' => [
                    'Тип' => $item['type'],
                    'Объем' => $item['value'].' Гб'
                ],
                'advancedModal' => [
                    'Брэнд' => $item['brend'],
                    'Модель' => $item['name'],
                    'Тип' => $item['type'],
                    'Объем' => $item['value'].' Гб',
                    'Частота' => $item['frequency'].' Mhz'
                ],
                'image' => $item['image'],
                'type' => 'ram',
                'id' => $key,
                'item' => $item
            ];
        }

        return $ram;
    }
}
