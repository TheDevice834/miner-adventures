<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class AccountController extends Controller
{
    private function getBitcoinMarketPlace(){
        $client = new Client();

        try {
            $result = $client->get('https://api.blockchain.info/stats');
            $array = $result->getBody();
            $array = json_decode($array, true);

            return $array['market_price_usd'];
        } catch(GuzzleException $exception){
            return 13000;
        }
    }

    public function account(){
    	return view('account.account', [
			'title' => 'Аккаунт',
			'user' => Auth::user()
		]);    
    }

    private function getDollarsFromBitcoins($bitcoins){
        $course = $this->getBitcoinMarketPlace();

    	$dollars = number_format($course * $bitcoins, 2);
        return str_replace(',', '', $dollars);
    }

    public function balance(){
    	$course = $this->getBitcoinMarketPlace();

    	$user = Auth::user();

    	$acceptToBuy = $this->getDollarsFromBitcoins($user->bitcoins);

    	$bitcoinCountText = 'биткоин';
    	$dollarsCountText = 'доллар';

		$number = substr($user->bitcoins, -2);
		if($number > 10 and $number < 15)
		{
			$bitcoinCountText .= "ов";
		}
		else
		{ 
			$number = substr($number, -1);
			if($number == 0) $bitcoinCountText .= "ов"; 
			else if($number > 1 ) $bitcoinCountText .= "а";
			else if($number > 4 ) $bitcoinCountText .= "ов";
		}

		$number = substr($user->dollars, -2);
		if($number > 10 and $number < 15)
		{
			$dollarsCountText .= "ов";
		}
		else
		{ 
			$number = substr($number, -1);
			if($number == 0) $dollarsCountText .= "ов"; 
			else if($number > 1 ) $dollarsCountText .= "а";
			else if($number > 4 ) $dollarsCountText .= "ов";
		}

		return view('account.balance', [
			'title' => 'Баланс',
			'user' => $user,
			'bitcoin_count_text' => $bitcoinCountText,
			'dollars_count_text' => $dollarsCountText,
			'course' => $course,
			'accept_to_buy' => $acceptToBuy
		]);    	
    }

    public function exchange(Request $request){
    	$rules = [
    		'bitcoins' => 'required|numeric'
    	];
    	$messages = [
    		'bitcoins.required' => 'Введите количество биткоинов на обмен',
    		'bitcoins.numeric' => 'Неверное количество биткоинов'
    	];

    	$validator = Validator::make($request->all(), $rules, $messages);

    	if($validator->fails()){
    		return redirect()->route('balance')
                ->withErrors($validator)
                ->withInput();
        }

        $user = Auth::user();

        if($user->bitcoins < $request->input('bitcoins')){
    		return redirect()->route('balance')
                ->withErrors(['Не хватает биткоинов'])
                ->withInput();
        }


    	$dollars = $this->getDollarsFromBitcoins($request->input('bitcoins'));

    	if($dollars < 0.01){
    		return redirect()->route('balance')
                ->withErrors(['Слишком малое число для обмена'])
                ->withInput();
    	}

    	$user->dollars += $dollars;
    	$user->bitcoins -= $request->input('bitcoins');

    	$user->save();

        return redirect()->route('balance');
    }
}
