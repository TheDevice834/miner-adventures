<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Farm;
use App\Videocard;

class FarmsController extends Controller
{
    public function index(){
        $farms = Farm::where('owner_id', Auth::user()['id'])->orderBy('id', 'desc')->get();

        $videocards_info = [];
        $farm_types = [];
        foreach($farms as $farm){
            $videocards_info[$farm->id] = $farm->getVideocardsInfo();
            $farm_types[$farm->id] = Farm::$farms[$farm->type_id];
        }

    	return view('farms.index', [
    		'title' => 'Фермы',
            'farms' => $farms,
            'farm_types' => $farm_types,
            'videocards_info' => $videocards_info
    	]);
    }

    public function view($farm_id){
        if(!$farm = Farm::find($farm_id)) abort(404);
        if($farm->owner_id != Auth::user()->id) abort(403);
        $videocards = Videocard::where('farm_id', $farm_id)->orderBy('id', 'desc')->get();

        $videocards_info = $farm->getVideocardsInfo();

        $videocard_types = [];
        foreach($videocards as $videocard){
            $videocard_types[$videocard->id] = Videocard::$videocards[$videocard->type_id];
        }

        $pc = [];
        if(isset(Farm::$motherboards[$farm->motherboard_id])){
            $pc['Материнская плата'] = Farm::$motherboards[$farm->motherboard_id]['brend'].' '.Farm::$motherboards[$farm->motherboard_id]['name'];
        } else $pc['Материнская плата'] = 0;

        if(isset(Farm::$processors[$farm->cpu_id])){
            $pc['Процессор'] = Farm::$processors[$farm->cpu_id]['brend'].' '.Farm::$processors[$farm->cpu_id]['name'];
        } else $pc['Процессор'] = 0;

        if(isset(Farm::$drives[$farm->drive_id])){
            $pc['Жетский диск'] = Farm::$drives[$farm->drive_id]['brend'].' '.Farm::$drives[$farm->drive_id]['name'];
        } else $pc['Жетский диск'] = 0;

        if(isset(Farm::$powersupplies[$farm->powersupply_id])){
            $pc['Блок питания'] = Farm::$powersupplies[$farm->powersupply_id]['brend'].' '.Farm::$powersupplies[$farm->powersupply_id]['name'];
        } else $pc['Блок питания'] = 0;

        if(isset(Farm::$ram[$farm->ram_id])){
            $pc['Оперативная память'] = Farm::$ram[$farm->ram_id]['brend'].' '.Farm::$ram[$farm->ram_id]['name'];
        } else $pc['Оперативная память'] = 0;

        return view('farms.view', [
            'title' => 'Ферма #'.$farm_id,
            'farm' => $farm,
            'farm_types' => Farm::$farms[$farm->type_id],
            'videocards' => $videocards,
            'videocard_types' => $videocard_types,
            'videocards_info' => $videocards_info,
            'pc' => $pc
        ]);
    }

    public function sellVideocard($farm_id, $videocard_id){
        if(!$farm = Farm::find($farm_id)) abort(404);
        if($farm->owner_id != Auth::user()->id) abort(403);

        if(!$videocard = Videocard::find($videocard_id)) abort(404);
        if($videocard->farm_id != $farm_id) abort(403);

        $user = Auth::user();
        $user->dollars += Videocard::$videocards[$videocard->type_id]['price'];
        $user->save();

        $videocard->delete();

        return redirect()->route('farms.view', ['farm_id' => $farm_id]);
    }
}
