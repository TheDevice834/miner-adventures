<?php

namespace App\Http\Controllers;

use App\Videocard;
use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Farm;
use Auth;

class AuthController extends Controller
{
    public function loginGet(){
    	return view('auth.login', [
    		'title' => 'Вход'
    	]);
    }

    public function loginPost(Request $request){
        $rules = [
            'name' => 'required',
            'password' => 'required',
        ];
        $messages = [
            'name.required' => 'Вы не указали логин',
            'password.required' => 'Вы не указали пароль'
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return redirect()->route('login')
                ->withErrors($validator)
                ->withInput();
        }

        if(Auth::attempt(['name' => $request->input('name'), 'password' => $request->input('password')], true))
            return redirect()->route('home');
        else return redirect()->route('login')
                ->withErrors(['errors' => ['Неверный логин или пароль']])
                ->withInput();
    }

    public function registerGet(){
    	return view('auth.register', [
    		'title' => 'Регистрация'
    	]);
    }

    public function registerPost(Request $request){
    	$rules = [
            'name' => 'required|unique:users,name|between:4,24',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|between:4,64',
            'password2' => 'required|same:password',
        ];
        $messages = [
            'name.required' => 'Вы не указали логин',
            'email.required' => 'Вы не указали адрес электронной почты',
            'password.required' => 'Вы не указали пароль',
            'password2.required' => 'Вы не повторили пароль',
            'name.unique' => 'Данный логин уже занят',
            'email.unique' => 'Данный адрес электронной почты уже занят',
            'email.email' => 'Введите валидный адрес электронной почты',
            'name.between' => 'Логин должен быть не короче 4-х символов и не длиннее 24-х символов',
            'password.between' => 'Пароль должен быть не короче 4-х символов и не длиннее 64-х символов',
            'password2.same' => 'Пароль должен совпадать',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
            return redirect()->route('register')
                ->withErrors($validator)
                ->withInput();
        }

        if($user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'bitcoins' => 0,
            'dollars' => 1000
        ])){
            if($farm = Farm::create([
                'type_id' => 0,
                'owner_id' => $user->id
            ])){
                Auth::login($user, true);
                return redirect()->route('home');
            }
            else return redirect()->route('register')
                    ->withErrors(['errors' => ['У-упс! Что-то пошло не так...']])
                    ->withInput();
        }
        else return redirect()->route('register')
                ->withErrors(['errors' => ['У-упс! Что-то пошло не так...']])
                ->withInput();
    }

    public function logoutGet(){
        Auth::logout();
        return redirect()->route('home');
    }
}
