<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(){
    	return view('main.index', [
    		'title' => 'Новости'
    	]);
    }

    public function about(){
    	return view('main.about', [
    		'title' => 'О нас'
    	]);
    }
}
