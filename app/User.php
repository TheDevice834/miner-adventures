<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'bitcoins', 'dollars'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function shortBalance(){
        $dollars = $this->dollars;
        $short = explode('.', $dollars)[0];

        $addK = '';
        while($short >= 1000){
            $short = substr($short, 0, -3);

            $addK .= 'K';
        }
        $short .= $addK;

        return $short;
    }
}
