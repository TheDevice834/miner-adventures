<?php

namespace App\Console\Commands;

use App\User;
use App\Farm;
use Illuminate\Console\Command;

class AddBitcoin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:bitcoin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add Bitcoins to users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for($i = 0; $i <= User::count(); $i++){
            if($user = User::find($i)){
                $farms = Farm::where('owner_id', $user->id)->get();

                if(count($farms) > 0) foreach($farms as $farm){
                    if($farm->isWork() === true){
                        $hashrate = $farm->getVideocardsInfo()['hashrate'];
                        $bitcoins = $hashrate / env('HASHRATE_TO_BITCOIN');
                        $bitcoins *= 0.0000000001;
                        $bitcoins *= 60;

                        $user->bitcoins += $bitcoins;
                        $user->save();
                    }
                }
            }
        }
    }
}
