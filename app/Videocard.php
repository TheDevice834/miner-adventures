<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Videocard extends Model
{
    public static $videocards = [
        0 => [
            'brend' => 'AMD',
            'model' => 'Radeon RX 470',
            'memory_value' => 8,
            'memory_type' => 'GDDR5',
            'gpu_frequency' => 926,
            'tdp' => 120,
            'hashrate' => 0.024,
            'price' => 195,
            'image' => 'id-0.jpg'
        ],
    ];

    protected $fillable = [
        'farm_id', 'type_id', 'riser'
    ];
}
