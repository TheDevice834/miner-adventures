<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFarms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('farms', function (Blueprint $table) {
            $table->integer('motherboard_id')->default(0);
            $table->integer('cpu_id')->default(0);
            $table->integer('drive_id')->default(0);
            $table->integer('powersupply_id')->default(0);
            $table->integer('ram_id')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('farms', function (Blueprint $table) {
            $table->dropColumn('motherboard_id');
            $table->dropColumn('cpu_id');
            $table->dropColumn('drive_id');
            $table->dropColumn('powersupply_id');
            $table->dropColumn('ram_id');
        });
    }
}
