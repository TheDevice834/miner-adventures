<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FullEditVideocardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videocards', function (Blueprint $table) {
            $table->integer('type_id');
            $table->dropColumn('name');
            $table->dropColumn('memory');
            $table->dropColumn('gpufrequency');
            $table->dropColumn('memoryfrequency');
            $table->dropColumn('hashrate');
            $table->dropColumn('tdp');
            $table->dropColumn('price');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videocards', function (Blueprint $table) {
            $table->dropColumn('type_id');
            $table->string('name');
            $table->integer('memory');
            $table->integer('gpufrequency');
            $table->integer('memoryfrequency');
            $table->integer('hashrate');
            $table->integer('tdp');
            $table->integer('price');
        });
    }
}
