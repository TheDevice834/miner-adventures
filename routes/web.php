<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'MainController@index']);
Route::get('/about', ['as' => 'about', 'uses' => 'MainController@about']);

Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@loginGet', 'middleware' => 'guest']);
Route::post('/login', ['as' => 'login', 'uses' => 'AuthController@loginPost', 'middleware' => 'guest']);
Route::get('/register', ['as' => 'register', 'uses' => 'AuthController@registerGet', 'middleware' => 'guest']);
Route::post('/register', ['as' => 'register', 'uses' => 'AuthController@registerPost', 'middleware' => 'guest']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logoutGet', 'middleware' => 'auth']);

Route::get('/shop', ['as' => 'shop', 'uses' => 'ShopController@index', 'middleware' => 'auth']);
Route::get('/shop/category/{category_name}', ['as' => 'shop.category', 'uses' => 'ShopController@category', 'middleware' => 'auth'])->where('category_name', '[a-zA-Z]+');
Route::get('/shop/buy/{type}/{id}', ['as' => 'shop.buy', 'uses' => 'ShopController@buy', 'middleware' => 'auth'])->where('type', '[a-zA-Z]+')->where('id', '[0-9]+');

Route::get('/farms', ['as' => 'farms', 'uses' => 'FarmsController@index', 'middleware' => 'auth']);
Route::get('/farms/{farm_id}', ['as' => 'farms.view', 'uses' => 'FarmsController@view', 'middleware' => 'auth'])->where('farm_id', '[0-9]+');
Route::get('/farms/{farm_id}/sell/videocard/{videocard_id}', ['as' => 'farms.sell.videocard', 'uses' => 'FarmsController@sellVideocard', 'middleware' => 'auth'])->where('farm_id', '[0-9]+')->where('videocard_id', '[0-9]+');

Route::get('/account', ['as' => 'account', 'uses' => 'AccountController@account', 'middleware' => 'auth']);
Route::get('/balance', ['as' => 'balance', 'uses' => 'AccountController@balance', 'middleware' => 'auth']);
Route::post('/balance/exchange', ['as' => 'balance.exchange', 'uses' => 'AccountController@exchange', 'middleware' => 'auth']);