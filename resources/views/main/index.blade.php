@extends('app')

@section('body')
	<div class="jumbotron page-body-jumbotron">
		<div class="container">
			<h1>{{ env('APP_NAME') }}</h1>
			<p>Скоро.</p>
			<p><a class="btn btn-primary btn-lg" href="{{ route('about') }}">Подробнее...</a></p>
		</div>
	</div>
	<div class="container">
		Скоро...
	</div>
@endsection