@extends('app')

@section('body')
	<div class="container page-body">
		@if(\Session::has('success'))
            <div class="alert alert-success">
                {!! \Session::get('success') !!}
            </div>
        @endif
			@if(\Session::has('error'))
				<div class="alert alert-danger">
					{!! \Session::get('error') !!}
				</div>
			@endif
		<div class="row">
			<div class="col-lg-3">
				<div class="list-group">
					@foreach($categories as $category)
						<a href="{{ $category['link'] }}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
							{{ $category['title'] }}
							<span class="badge badge-primary badge-pill">{{ $category['count'] }}</span>
						</a>
					@endforeach
				</div>
			</div>
			<div class="col-lg-9">
				@if(count($items) > 0)
					<div class="row">
						@foreach($items as $key => $item)
							<div class="col-lg-4 card-margin-bottom">
								<div class="card">
									<div class="card-header">
										{{ $item['name'] }}
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">
											<img class="shop-image" @if($item['type'] == 'ram') style="height: 50px;" @endif src="/assets/images/{{ $item['type'] }}/{{ $item['image'] }}" alt="{{ $item['name'] }}">
										</li>
										<li class="list-group-item">Категория: <b><a href="{{ $categories[$item['category']]['link'] }}">{{ $categories[$item['category']]['title'] }}</a></b></li>
										@foreach($item['advanced'] as $a_key => $advanced)
											<li class="list-group-item">{{ $a_key }}: <b>{{ $advanced }}</b></li>
										@endforeach
									</ul>
									<div class="card-footer">
										<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#itemModal{{ $key }}">
											Подробнее &bull; <b>${{ $item['price'] }}</b>
										</button>
									</div>
								</div>
							</div>
						@endforeach
					</div>
					@foreach($items as $key => $item)
						<div class="modal fade" id="itemModal{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">{{ $item['name'] }}</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<img width="100%" src="/assets/images/{{ $item['type'] }}/{{ $item['image'] }}" alt="{{ $item['name'] }}">
										<hr>
										@foreach($item['advancedModal'] as $a_key => $advanced)
											{{ $a_key }}: <b>{{ $advanced }}</b><br>
										@endforeach
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
										<a href="{{ route('shop.buy', ['type' => $item['type'], 'id' => $item['id']]) }}" class="btn btn-primary">Купить за <b>${{ $item['price'] }}</b></a>
									</div>
								</div>
							</div>
						</div>
					@endforeach
				@else
					<div class="alert alert-info">
						Товаров не найдено.
					</div>
				@endif
			</div>
		</div>
	</div>
@endsection
