@extends('app')

@section('body')
	<div class="container page-body">
		<div class="card">
			<div class="card-body">
				Выберите ферму, на которую будет {{ $text }}.
				<br><br>
				<form>
					<select name="farm" class="custom-select">
						@foreach($farms as $farm)
							<option value="{{ $farm->id }}">#{{ $farm->id }} &laquo;{{ $types[$farm->type_id]['name'] }}&raquo;</option>
						@endforeach
					</select>
					<hr>
					<button type="submit" class="btn btn-primary">Купить</button>
				</form>
			</div>
		</div>
	</div>
@endsection