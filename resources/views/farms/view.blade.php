@extends('app')

@section('body')
	<div class="container page-body">
		<a href="{{ route('farms') }}" class="btn btn-primary btn-block">Мои фермы</a>
		<hr>
		<h2 class="text-center">Ферма #{{ $farm->id }}</h2>
		<br>
		<div class="card">
			<div class="card-header">Общая информация</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Тип: <b>{{ $farm_types['name'] }}</b></li>
				<li class="list-group-item">Кол-во видеокарт: <b>{{ $videocards_info['count'] }}</b></li>
				<li class="list-group-item">Общий хэшрейт: <b>{{ $videocards_info['hashrate'] }} Gh/s</b></li>
				<li class="list-group-item">Энергопотребление: <b>{{ $videocards_info['tdp'] }} Ватт</b></li>
			</ul>
		</div>
		<br>
		<div class="card">
			<div class="card-header">Комплектация</div>
			<ul class="list-group list-group-flush">
				@foreach($pc as $key => $item)
					<li class="list-group-item">
						{{ $key }}: <b>
							@if(strval($item))
								{{ $item }}
							@else
								<span class="text-red">Не установлено</span>
							@endif
						</b>
					</li>
				@endforeach
			</ul>
		</div>
		<br><br>
		<h4>Видеокарты:</h4>
		<hr>
		<div class="row">
			@if(count($videocards) > 0)
				@foreach($videocards as $videocard)
					<div class="col-lg-3">
						<div class="card card-margin-bottom">
							<div class="card-header">
								{{ $videocard_types[$videocard->id]['brend'] }} {{ $videocard_types[$videocard->id]['model'] }}
							</div>
							<ul class="list-group list-group-flush">
								<li class="list-group-item text-center">
									<img class="shop-image" src="/assets/images/videocards/{{ $videocard_types[$videocard->id]['image'] }}">
								</li>
								<li class="list-group-item">ID: <b>{{ $videocard->id }}</b></li>
								<li class="list-group-item">TDP: <b>{{ $videocard_types[$videocard->id]['tdp'] }} Ватт</b></li>
								<li class="list-group-item">Хэшрейт: <b>{{ $videocard_types[$videocard->id]['hashrate'] }} GH/s</b></li>
								<li class="list-group-item">Цена: <b>${{ $videocard_types[$videocard->id]['price'] }}</b></li>
							</ul>
							<div class="card-body">
								<a href="{{ route('farms.sell.videocard', ['farm_id' => $farm->id, 'videocard_id' => $videocard->id]) }}" class="btn btn-danger btn-block">Продать</a>
							</div>
						</div>
					</div>
				@endforeach
			@else
				<div class="col-lg-12">
					<div class="alert alert-info">
						На данной ферме нет видеокарт
					</div>
				</div>
			@endif
		</div>
	</div>
@endsection