@extends('app')

@section('body')
	<div class="container page-body text-center">
		<a href="{{ route('balance') }}" class="btn btn-warning">
			<i class="fa fa-btc fa-4x"></i>
			<br>{{ Auth::user()->bitcoins }}
			<br>
			<small>На Вашем счету</small>
		</a>
	</div>
	<br>
	<div class="container">
		<div class="alert alert-info">
			<b>Пометка:</b><br>
			<i><b>0.008 Gh/s</b> = <b>0.0000000001 BTC/s (0.000000006 BTC/min)</b></i>
			<br><br>
			Биткоины выдаются каждую минуту.
		</div>
	</div>
	<br>
	<div class="container">
		<h2>Фермы:</h2>
		<hr>
		<div class="row">
			@if(count($farms) > 0)
				@foreach($farms as $farm)
					<div class="col-lg-12">
						<div class="card card-margin-bottom">
							<div class="card-header">
								{{ $farm_types[$farm->id]['name'] }}
							</div>
							<ul class="list-group list-group-flush">
								<li class="list-group-item">Номер: <b>{{ $farm->id }}</b></li>
								<li class="list-group-item">Кол-во видеокарт: <b>{{ $videocards_info[$farm->id]['count'] }}</b></li>
								<li class="list-group-item">Общий хэшрейт: <b>{{ $videocards_info[$farm->id]['hashrate'] }} Gh/s</b></li>
								<li class="list-group-item">Энергопотребление: <b>{{ $videocards_info[$farm->id]['tdp'] }} Ватт</b></li>
							</ul>
							<div class="card-body">
								@php
									$isWork = $farm->isWork();
								@endphp
								@if($isWork === true)
									<a href="{{ route('farms.view', ['farm_id' => $farm->id]) }}" class="btn btn-success btn-block">
										Перейти &raquo;
										<br><small>Работает</small>
									</a>
								@else
									<a href="{{ route('farms.view', ['farm_id' => $farm->id]) }}" class="btn btn-danger btn-block">
										Перейти &raquo;
										<br><small>Не работает: {{ $isWork }}</small>
									</a>
								@endif
							</div>
						</div>
					</div>
				@endforeach
			@else
				<div class="alert alert-info">
					У вас нет ферм
					<br>
					<b><a href="{{ route('shop') }}">Купить</a></b>
				</div>
			@endif
		</div>
	</div>
@endsection

@section('scripts')
	<script>

	</script>
@endsection