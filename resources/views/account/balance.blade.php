@extends('app')

@section('body')
	<div class="container page-body">
		@if($errors->any())
			@foreach ($errors->all() as $error)
				<div class="alert alert-danger">{{ $error }}</div>
			@endforeach
		@endif
		<hr>
		<div class="card">
			<div class="card-header">
				У вас сейчас в наличии:
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item"><b>{{ $user->bitcoins }}</b> {{ $bitcoin_count_text }}</li>
				<li class="list-group-item"><b>{{ $user->dollars }}</b> {{ $dollars_count_text }}</li>
			</ul>
			<div class="card-body">
				<a href="#" class="btn btn-outline-success disabled"><i class="fa fa-credit-card"></i> Пополнить</a>
			</div>
		</div>
		<hr>
		<div class="card">
			<div class="card-header">
				Обменник:
			</div>
			<ul class="list-group list-group-flush">
				<li class="list-group-item">Стоимость биткоина: <b>${{ $course }}</b></li>
				<li class="list-group-item">
					<div class="alert alert-info" style="margin-bottom: 0px;">
						<i class="fa fa-btc"></i> <b>{{ $user->bitcoins }}</b> можно обменять на <b>${{ $accept_to_buy }}</b>
					</div>
				</li>
				<form method="post" action="{{ route('balance.exchange') }}">
					{{ csrf_field() }}
					<li class="list-group-item">
						<div class="input-group">
							<div class="input-group-addon"><i class="fa fa-btc"></i></div>
							<input type="text" name="bitcoins" class="form-control" placeholder="Количество биткоинов на обмен">
						</div>
					</li>
					</ul>
					<div class="card-body">
						<button type="submit" class="btn btn-warning"><i class="fa fa-refresh"></i> Обменять</button>
					</div>
				</form>
		</div>
	</div>
@endsection