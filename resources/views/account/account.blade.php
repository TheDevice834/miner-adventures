@extends('app')

@section('body')
	<div class="container page-body">
		<div class="card">
			<div class="card-header">
				Общая информация
			</div>
			<div class="card-body">
				Номер аккаунта: <b>{{ $user->id }}</b><br>
				Логин: <b>{{ $user->name }}</b><br>
				Адрес электронной почты: <b>{{ $user->email }}</b><br><br>
				Дата регистрации: <b>{{ $user->created_at }}</b>
			</div>
		</div>
	</div>
@endsection