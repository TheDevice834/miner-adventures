@extends('app')

@section('body')
	<div class="container page-body">
		@if ($errors->any())
			@foreach ($errors->all() as $error)
				<div class="alert alert-danger">{{ $error }}</div>
			@endforeach
		@endif
		<form method="post">
			{{ csrf_field() }}
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Логин</label>
				<div class="col-sm-10">
					<input name="name" class="form-control" placeholder="Логин">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Пароль</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control" placeholder="Пароль">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-10 offset-md-2">
					<button type="submit" class="btn btn-outline-secondary"><i class="fa fa-sign-in"></i> Войти</button>
				</div>
			</div>
		</form>
	</div>
@endsection