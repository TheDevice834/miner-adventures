@extends('app')

@section('body')
	<div class="container page-body">
		@if ($errors->any())
			@foreach ($errors->all() as $error)
				<div class="alert alert-danger">{{ $error }}</div>
			@endforeach
		@endif
		<form method="post">
			{{ csrf_field() }}
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Логин</label>
				<div class="col-sm-10">
					<input name="name" class="form-control" placeholder="Логин">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">E-Mail</label>
				<div class="col-sm-10">
					<input type="email" name="email" class="form-control" placeholder="Адрес электронной почты">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Пароль</label>
				<div class="col-sm-10">
					<input type="password" name="password" class="form-control" placeholder="Пароль">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Повторите пароль</label>
				<div class="col-sm-10">
					<input type="password" name="password2" class="form-control" placeholder="Повторите пароль">
				</div>
			</div>
			<div class="form-group row">
				<div class="col-sm-10 offset-md-2">
					<button type="submit" class="btn btn-outline-primary"><i class="fa fa-user-plus"></i> Зарегистрироваться</button>
				</div>
			</div>
		</form>
	</div>
@endsection
