<!DOCTYPE html>
<html lang='ru'>
<head>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ $title }} / {{ env('APP_NAME') }}</title>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Sans:500">
	<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="/assets/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="/assets/template/css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
		<a class="navbar-brand" href="{{ route('home') }}">{{ env('APP_NAME') }}</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item{{ Route::current()->getName() === 'home' ? ' active' : '' }}">
					<a class="nav-link" href="{{ route('home') }}"><i class="fa fa-home"></i> Главная</a>
				</li>
				<li class="nav-item{{ Route::current()->getName() === 'shop' ? ' active' : '' }}">
					<a class="nav-link" href="{{ route('shop') }}"><i class="fa fa-shopping-cart"></i> Магазин</a>
				</li>
				<li class="nav-item{{ Route::current()->getName() === 'farms' ? ' active' : '' }}">
					<a class="nav-link" href="{{ route('farms') }}"><i class="fa fa-server"></i> Фермы</a>
				</li>
			</ul>
			@if(Auth::check())
				<ul class="navbar-nav navbar-tpl">
					<li class="nav-item{{ Route::current()->getName() === 'account' ? ' active' : '' }}">
						<a class="nav-link" href="{{ route('account') }}"><i class="fa fa-user"></i> Аккаунт</a>
					</li>
					<li class="nav-item{{ Route::current()->getName() === 'balance' ? ' active' : '' }}">
						<a class="nav-link" href="{{ route('balance') }}">${{ Auth::user()->shortBalance() }}</a>
					</li>
				</ul>
				<div class="form-inline my-2 my-lg-0">
					<a href="{{ route('logout') }}" class="btn btn-outline-danger my-2 my-sm-0">Выйти</a>
				</div>
			@else
				<div class="form-inline my-2 my-lg-0">
					<div class="btn-group">
						<a href="{{ route('login') }}" class="btn btn-outline-secondary my-2 my-sm-0"><i class="fa fa-sign-in"></i> Вход</a>
						<a href="{{ route('register') }}" class="btn btn-outline-primary my-2 my-sm-0"><i class="fa fa-user-plus"></i> Регистрация</a>
					</div>
				</div>
			@endif
		</div>
	</nav>
	@yield('body')
	<div class="container">
		<hr>
		&copy 2017 <b><a href="{{ env('HOME_URL') }}">{{ env('APP_NAME') }}</a></b>
		<span class="pull-right">Powered by <b><a href="https://raveoncode.ru" target="_blank">Rave On Code</a></b></span>
	</div>
	<script src="/assets/js/jquery-3.2.1.slim.min.js"></script>
	<script src="/assets/js/popper.min.js"></script>
	<script src="/assets/bootstrap/js/bootstrap.min.js"></script>
	@yield('scripts')
</body>
</html>
